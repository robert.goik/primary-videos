import React from "react";

import logo from "../../assets/logo.png";
import "./Footer.css"

export const Footer = () => {
  return (
    <footer className="Footer">
      <img className="Footer-logo" src={logo} />
    </footer>
  );
};
