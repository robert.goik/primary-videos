import { createApp } from './index'

export const mount = (el) => {
  createApp(el);
};