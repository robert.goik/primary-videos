# How NOT to build micro frontends!

AWS S3, CloudFront and Amazon Prime Mini Clone

## How to start

copy envs in `core-aws-mf`

```
cd core-aws-mf
cp .env.example .env
```

Run each app (in every app folder, yes this is by designe, check out the title ;-))

```
cd APP_DIR_NAME
npm start
```

## How tu build

```
npm build
```

# WARNING

```
Every push to repository will trigger pipelines (deployment to S3 and CloudFront). Don't worry, If envs will not be provided, it will just fail :)
```
