export const mount = async (el) => {
  const module = () => import("./app");
  const { vm } = await module();
  return vm.$mount(el);
};