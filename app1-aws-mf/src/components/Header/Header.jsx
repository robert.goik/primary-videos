import React from "react";

import logo from "../../assets/logo.png";
import search from "../../assets/search.svg";
import user from "../../assets/user.png";
import "./Header.css";


export const Header = () => {
  return (
    <header className="Header">
      <div className="Header-container">
        <img className="Header-logo" src={logo} />
        <div className="Header-nav">
          <ul>
            <li>
              <a href="#" className="Header-nav-link">
                Main page
              </a>
            </li>
          </ul>
        </div>
        <input
          className="Header-search"
          style={{ backgroundImage: `url(${search})` }}
          type="search"
          placeholder="Search"
        ></input>
        <img className="Header-user" src={user} />
        <div className="Header-user-name">
          <span>user</span>
        </div>
      </div>
    </header>
  );
};
