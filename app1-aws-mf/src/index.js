import React from 'react';
import ReactDOM from 'react-dom';
import './app.css';
import App from './App';

export const createApp = (el) => {
  ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    document.querySelector(el)
  )
}
