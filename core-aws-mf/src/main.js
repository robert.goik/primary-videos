import { Loader } from "./loader"

const headerlLoader = new Loader(process.env.SCRIPT_APP_1, "app1")
const carouselLoader = new Loader(process.env.SCRIPT_APP_2, "app2")
const sliderlLoader = new Loader(process.env.SCRIPT_APP_3, "app3")
const footerlLoader = new Loader(process.env.SCRIPT_APP_4, "app4")

headerlLoader.init("main", "#app1")
carouselLoader.init("main", "#app2")
sliderlLoader.init("main", "#app3")
footerlLoader.init("main", "#app4")