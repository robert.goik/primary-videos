export class Loader {
  constructor(remoteEntry, container) {
    this.remoteEntry = remoteEntry;
    this.container = container;
  }

  init(component, selector) {
    const remoteScript = document.createElement("script");
    remoteScript.setAttribute("src", this.remoteEntry);
    document.head.appendChild(remoteScript);

    remoteScript.addEventListener("load", async () => {
      await __webpack_init_sharing__("default");

      const container = window[this.container];
      await container.init(__webpack_share_scopes__.default);

      const factoryFn = await container.get(`./${component}`);
      const module = factoryFn();

      await module.mount(selector)
    })
  }
}